const express = require('express');
const monk = require('monk');

const { body, validationResult, Result } = require('express-validator');

const router = express.Router();

// Connection URL
const url = 'localhost:27017/AuthJWT';
const db = monk(url);
const user = db.get('Users');

router.get('/', (req, res) => {
    // res.send('Register User !!');
    // res.json(user.find({}).then((docs) => {console.log(docs)}));
    user.find({}).then((data) => {
        // only the name field will be selected
        res.send({
            users: data
        });
    });
});

router.post('/add', [
        body('username', "Please Input your product username").not().isEmpty(),
        body('email', "Please Input your product email").not().isEmpty(),
        body('password', "Please Input your product password").not().isEmpty(),
    ], (req, res) => {
    user.insert({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    }, (err) =>{
        if(err){
            res.send(err);
        }else{
            res.send('Successfully !!');
        }
    });
});

module.exports = router;