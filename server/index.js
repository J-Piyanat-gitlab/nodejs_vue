const express = require('express');
const body_parser = require('body-parser');
const cors = require('cors');

const app = express();

//Middleware
app.use(body_parser.json());
app.use(cors());

const register = require('./routes/api/auth/register');

app.use('/register', register);

const port = process.env.PORT || 3000

app.listen(port, () => console.log(`Server started on port ${port}`));
